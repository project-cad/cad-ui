import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Button, Dropdown, DropdownItem, DropdownToggle, DropdownMenu} from 'reactstrap';
import {API_URL} from "../constants";
import {SET_USER} from "../redux/actions";
import {withRouter, Route, Link} from 'react-router-dom';
import UserHome from './UserHome';
import EditUserForm from './EditUserForm';
import UserSearchForm from "./UserSearchForm";
import ServerManagement from "./Servers/ServerManagement";

class UserDashboard extends Component {
    constructor() {
        super();
        this.state = {userDropOpen: false}
        this.toggleUserDrop = this.toggleUserDrop.bind(this);
        this.logout = this.logout.bind(this);
        this.navTo = this.navTo.bind(this);
    }

    toggleUserDrop() {
        this.setState({userDropOpen: !this.state.userDropOpen});
    }

    logout() {
        fetch(API_URL + "/auth/logout", {credentials: "include"}).then(e => e.json()).catch(e => console.error(e))
            .then(e => {
                this.props.dispatch({type: SET_USER, user: null});
            });
    }

    navTo(to) {
        this.props.history.push(to);
    }

    render() {
        let userIsAdmin = this.props.user.roles.indexOf('Admin') !== -1;
        return (
            <div className="container mt-2">
                <div className={"row"}>
                    <div className={"col-6"}>
                        <Link to={"/"}><strong>RPCAD</strong></Link>
                    </div>
                    <div className={"col-6"}>
                        <div className={"d-flex justify-content-end"}>
                            <Dropdown isOpen={this.state.userDropOpen} toggle={this.toggleUserDrop} size={"sm"}>
                                <DropdownToggle caret>
                                    {this.props.user.name} ({this.props.user.identifier_type} {this.props.user.identifier})
                                </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem header>General</DropdownItem>
                                    <DropdownItem onClick={() => this.navTo("/")}>Home</DropdownItem>
                                    <DropdownItem onClick={() => this.navTo("/me")}>Edit Account</DropdownItem>
                                    {userIsAdmin ? <DropdownItem header>Administration</DropdownItem> : ""}
                                    {userIsAdmin ? <DropdownItem onClick={() => this.navTo("/users")}>Users</DropdownItem> : ""}
                                    {userIsAdmin ? <DropdownItem onClick={() => this.navTo("/servers")}>Servers</DropdownItem> : ""}
                                    {userIsAdmin ? <DropdownItem>Roles</DropdownItem> : ""}
                                    <DropdownItem divider />
                                    <DropdownItem onClick={this.logout}>Logout</DropdownItem>
                                </DropdownMenu>
                            </Dropdown>
                        </div>
                    </div>
                </div>
                <div className={"mt-5"}>
                    <Route exact path={"/"} component={UserHome} />
                    <Route exact path={"/me"} component={EditUserForm} />
                    <Route exact path={"/users"} component={UserSearchForm} />
                    <Route path={"/servers"} component={ServerManagement} />
                </div>
            </div>
        );
    }
}

let mstp = state => {
    return {
        user: state.user
    }
};

export default withRouter(connect(mstp)(UserDashboard));