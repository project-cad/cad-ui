import React, {Component} from 'react';
import {Form, Button, FormGroup, Label, Input, Alert} from 'reactstrap';
import {connect} from 'react-redux';
import {API_URL} from "../constants";
import {SET_USER} from "../redux/actions";

const errorcodes = {
    400: "An internal error occured.",
    401: "Your session has expired.",
    403: "Your current password does not match.",
    409: "There was a conflict with your identifier or identifier type."
};

class EditUserForm extends Component {
    constructor() {
        super();
        this.state = {message: null, message_color: "info"};
        this.verifyAndSend = this.verifyAndSend.bind(this);
        this.email = null;
        this.id_type = null;
        this.ident = null;
        this.new_pass = null;
        this.confirm_new_pass = null;
        this.current_pass = null;
    }

    verifyAndSend() {
        let FD = new FormData();
        if(this.email.value !== this.props.user.email) FD.append("email", this.email.value);
        if(this.new_pass.value !== "") {
            FD.append("current_password", this.current_pass.value);
            FD.append("new_password", this.new_pass.value);
        }
        if(this.ident.value !== this.props.user.identifier) FD.append("identifier", this.ident.value);
        if(this.id_type.value !== this.props.user.identifier_type) FD.append("identifier_type", this.id_type.value);
        if(this.new_pass.value !== this.confirm_new_pass.value) {
            this.setState({message: "The new password and confirm password fields do not match.", message_color: "danger"});
            return;
        }
        fetch(API_URL + "/account", {
            method: "POST",
            credentials: "include",
            body: FD
        }).then(resp => {
            if(resp.status !== 200) {
                this.setState({message: errorcodes[resp.status], message_color: "danger"});
            }
            return resp.json();
        }).catch(e => console.error(e))
            .then(e => {
                if(e.status === "error") return;
                this.props.dispatch({type: SET_USER, user: e.user});
                this.setState({message: "Your profile was successfully saved.", message_color: "success"});
            });
    }

    render() {
        return (
            <div>
                <h1>Your Account</h1>
                <hr />
                <Form>
                    {this.state.message != null ? <Alert color={this.state.message_color} className={"mb-3"}>
                        {this.state.message}
                    </Alert> : ""}
                    <div className={"row"}>
                        <div className={"col-md-4"}>
                            <FormGroup>
                                <Label for={"email"}>Email Address</Label>
                                <Input type={"email"} required id={"email"} defaultValue={this.props.user.email} innerRef={(e) => this.email = e} />
                            </FormGroup>
                        </div>
                        <div className={"col-md-4"}>
                            <FormGroup>
                                <Label for={"identifier-type"}>Identifier Type</Label>
                                <Input type={"select"} id={"identifier-type"} defaultValue={this.props.user.identifier_type} innerRef={(e) => this.id_type = e}>
                                    <option value={"LEO"}>Law Enforcement Officer</option>
                                    <option value={"Civ"}>Civilian</option>
                                    <option>Dispatcher</option>
                                </Input>
                            </FormGroup>
                        </div>
                        <div className={"col-md-4"}>
                            <FormGroup>
                                <Label for={"identifier"}>Identifier</Label>
                                <Input type={"number"} required id={"identifier"} defaultValue={this.props.user.identifier} innerRef={(e) => this.ident = e}/>
                            </FormGroup>
                        </div>
                    </div>
                    <div className={"row"}>
                        <div className={"col-md-4"}>
                            <FormGroup>
                                <Label for={"new_pass"}>New Password</Label>
                                <Input type={"password"} id={"new_pass"} innerRef={(e) => this.new_pass = e} />
                            </FormGroup>
                        </div>
                        <div className={"col-md-4"}>
                            <FormGroup>
                                <Label for={"confirm_new_pass"}>Confirm New Password</Label>
                                <Input type={"password"} id={"password"} innerRef={(e) => this.confirm_new_pass = e} />
                            </FormGroup>
                        </div>
                        <div className={"col-md-4"}>
                            <FormGroup>
                                <Label for={"current_pass"}>Current Password</Label>
                                <Input type={"password"} id={"current_pass"} innerRef={(e) => this.current_pass = e} />
                            </FormGroup>
                        </div>
                    </div>
                    <div className={"mt-2 d-flex justify-content-end"}>
                        <Button color={"primary"} onClick={this.verifyAndSend}>Save</Button>
                    </div>
                </Form>
            </div>
        );
    }
}

const mstp = state => {
    return {
        user: state.user
    }
};

export default connect(mstp)(EditUserForm);