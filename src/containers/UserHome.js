import React, {Component} from 'react';
import {Button, Table} from 'reactstrap';
import {connect} from 'react-redux';
import {API_URL} from "../constants";
import ServerList from '../components/ServerList';

class UserHome extends Component {
    constructor() {
        super();
        this.state = {servers: []};
    }
    componentDidMount() {
        fetch(API_URL + "/servers", {credentials: "include"})
            .then(e => e.json())
            .catch(e => console.log(e))
            .then(resp => {
                this.setState({servers: resp.servers});
            });
    }
    render() {
        return <div>
            <div className={"d-flex justify-content-center mb-3"}>
                <h1 className={"display-4"}>Welcome back, {this.props.user.name}</h1>
            </div>
            <div className={"d-flex justify-content-center"}>
                <Button color={"primary"} size={"lg"}>Start Patrol</Button>
            </div>
            <div className={"row mt-5"}>
                <div className={"col-md-4"}>
                    <ServerList servers={this.state.servers} publicList={true} />
                </div>
            </div>
        </div>;
    }
}

const mstp = state => {
    return {
        user: state.user
    }
};

export default connect(mstp)(UserHome);