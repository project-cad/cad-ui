import React, {Component} from 'react';
import {API_URL} from "../../constants";
import ServerList from '../../components/ServerList';
import {Route} from 'react-router-dom';
import ServersHome from "./ServersHome";
import {Nav, NavItem, NavLink} from 'reactstrap';
import NewServer from "./NewServer";

class ServerManagement extends Component {
    constructor() {
        super();
        this.state = {servers: []};
        this.navTo = this.navTo.bind(this);
    }

    componentDidMount() {

    }

    navTo(a) {
        this.props.history.push(a);
    }

    render() {
        return (
            <div>
                <h1>Server Management</h1>
                <hr />
                <div className={"row"}>
                    <div className={"col-md-3"}>
                        <Nav vertical pills>
                            <NavItem>
                                <NavLink href={"#"} active={this.props.location.pathname === "/servers"} onClick={() => this.navTo("/servers")}>Servers</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href={"#"} active={this.props.location.pathname === "/servers/new"} onClick={() => this.navTo("/servers/new")}>+ New Server</NavLink>
                            </NavItem>
                        </Nav>
                    </div>
                    <div className={"col-md-9"}>
                        <Route exact path={"/servers"} component={ServersHome} />
                        <Route exact path={"/servers/new"} component={NewServer} />
                    </div>
                </div>
            </div>
        );
    }
}

export default ServerManagement;