import React, {Component} from 'react';
import {Form, FormGroup, Input, Button, Label} from 'reactstrap';
import {API_URL} from "../../constants";
import {Alert} from "reactstrap";

class NewServer extends Component {
    constructor() {
        super();
        this.name = null;
        this.create = this.create.bind(this);
        this.state = {message: null, message_color: "info"};
    }
    create() {
        let FD = new FormData();
        FD.append("name", this.name.value);
        fetch(API_URL + "/servers", {
            method: "POST",
            body: FD,
            credentials: "include"
        }).then(e => {
            if(e.status === 403) {
                this.setState({message: "You don't have permission to create servers.", message_color: "danger"});
            }
            return e.json();
            })
            .catch(e => console.error(e))
            .then(e => {
                if(e.status === "error") return;
                this.setState({message: "Server created successfully.", message_color: "success"});
                this.name.value = "";
            });
    }
    render() {
        return <div>
            <Form>
                {this.state.message != null ? <Alert color={this.state.message_color} className={"mb-3"}>
                    {this.state.message}
                </Alert> : ""}
                <div className={"row"}>
                    <div className={"col-md-12"}>
                        <FormGroup>
                            <Label for={"name"}>Server Name</Label>
                            <Input type={"text"} id={"name"} innerRef={(e) => this.name = e} autoComplete={"off"} />
                        </FormGroup>
                    </div>
                </div>
                <div className="d-flex justify-content-end">
                    <Button color={"primary"} onClick={this.create} outline>Add</Button>
                </div>
            </Form>
        </div>;
    }
}

export default NewServer;