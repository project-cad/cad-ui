import React, {Component} from 'react';
import ServerList from "../../components/ServerList";
import {Button} from "reactstrap";
import {API_URL} from "../../constants";

class ServersHome extends Component {
    constructor() {
        super();
        this.state = {servers: []};
    }
    componentDidMount() {
        fetch(API_URL + "/servers", {credentials: "include"})
            .then(e => e.json())
            .catch(e => console.error(e))
            .then(e => {
                this.setState({servers: e.servers});
            });
    }
    render() {
        return (
            <div>
                <ServerList servers={this.state.servers} />
            </div>
        );
    }
}

export default ServersHome