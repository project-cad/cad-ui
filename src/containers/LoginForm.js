import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Form, FormGroup, Label, Input, Button, Alert} from 'reactstrap';
import {API_URL} from '../constants';
import {SET_USER} from "../redux/actions";

function toQS(params) {
    return Object.keys(params).map(key => key + '=' + params[key]).join('&');
}

class LoginForm extends Component {
    constructor() {
        super();
        this.login = this.login.bind(this);
        this.username = null;
        this.password = null;
        this.state = {message: null, message_color: "info"}
    }
    login(e) {
        this.setState({message: "Please wait... Exchanging data with the session provider.", message_color: "info"});
        let FD = new FormData();
        FD.append('email', document.getElementById("email").value);
        FD.append('password', document.getElementById("password").value);
        fetch(API_URL + "/auth", {
            method: "POST",
            credentials: "include",
            body: FD
        }).then((res) => res.json())
            .catch(error => {
                console.error(error);
                this.setState({message: "An internal error occured.", message_color: "danger"});
            })
            .then(response => {
                if(response.status === "error") {
                    this.setState({message: "Invalid email or password.", message_color: "danger"});
                    return;
                }
                fetch(API_URL + "/account", {credentials: "include"}).then(res => res.json()).catch(error => console.error(error))
                    .then((e) => {
                        this.props.dispatch({type: SET_USER, user: e.user});
                    })
            });
    }
    render() {
        return (<div className={"container"}>
            <div className={"row"}>
                <div className={"col-md-4"}>

                </div>
                <div className={"col-md-4 mt-5"}>
                    {this.state.message != null ? <Alert color={this.state.message_color} className={"mb-3"}>
                            {this.state.message}
                    </Alert> : ""}
                    <Form>
                        <h3>Login to RPCAD</h3>
                        <FormGroup>
                            <Label for={"email"}>Email Address</Label>
                            <Input type={"email"} name={"email"} id={"email"} autoComplete={"off"} ref={(e) => this.username = e} />
                        </FormGroup>
                        <FormGroup>
                            <Label for={"password"}>Password</Label>
                            <Input type={"password"} name={"password"} id={"password"} autoComplete={"off"} ref={(e) => this.password = e} />
                        </FormGroup>
                        <Button color={"primary"} onClick={this.login}>Login</Button>
                    </Form>
                </div>
            </div>
        </div>);
    }
}

export default connect()(LoginForm);