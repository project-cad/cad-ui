import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';
import Reducers from './redux/reducers';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import 'whatwg-fetch';

const store = createStore(Reducers);

ReactDOM.render(<Provider store={store}>
    <App />
</Provider>, document.getElementById('root'));
