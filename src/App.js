import React, {Component} from 'react';
import {connect} from 'react-redux';
import LoginForm from './containers/LoginForm';
import UserDashboard from './containers/UserDashboard';
import {API_URL} from './constants';
import {SET_USER} from "./redux/actions";
import {BrowserRouter} from 'react-router-dom';

/**
 * This is the main class that determines which components are to be shown.
 */

class App extends Component {
    constructor() {
        super();
    }
    componentDidMount() {
        // Should be called on startup, check for logged-in user
        fetch(API_URL + "/account", {credentials: "include"}).then(res => res.json()).catch(error => console.error(error))
            .then((e) => {
                if(e.status === "error") return;
                this.props.dispatch({type: SET_USER, user: e.user});
            })
    }
    render() {
        if(this.props.user == null) {
            return <LoginForm />;
        }
        return (
            <BrowserRouter>
                <UserDashboard/>
            </BrowserRouter>
        );
    }
}

const mstp = (state) => {
    return {
        user: state.user
    }
};

export default connect(mstp)(App);
