import React, {Component} from 'react';
import {Input} from 'reactstrap';

class EditableField extends Component {
    constructor() {
        super();
        this.input = null;
        this.text = null;
        this.processChange = this.processChange.bind(this);
        this.KD = this.KD.bind(this);
    }
    componentDidMount() {
        this.text = this.props.text;
    }
    componentDidUpdate() {
        if(this.props.editable) this.input.focus();
    }
    processChange() {
        this.text = this.input.value;
        this.props.OnChange(this.input.value);
    }
    KD(event) {
        if(event.keyCode === 13) this.processChange();
    }
    render() {
        if(!this.props.editable) return <span>{this.text == null ? this.props.text : this.text}</span>;
        return <Input innerRef={(e) => this.input = e} defaultValue={this.text == null ? this.props.text : this.text} onBlur={this.processChange} onKeyDown={this.KD} />
    }
}

export default EditableField;