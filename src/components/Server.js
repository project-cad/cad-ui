import React, {Component} from 'react';
import EditableField from '../components/EditableField'
import {API_URL} from "../constants";
import {Button} from 'reactstrap';
import swal from 'sweetalert2';

class Server extends Component {
    constructor() {
        super();
        this.state = {nameEdit: false, deleted: false};
        this.changed = this.changed.bind(this);
        this.delete = this.delete.bind(this);
    }
    changed(newVal) {
        var FD = new FormData();
        FD.append("name", newVal);
        if(newVal === this.props.server.name) {
            this.setState({nameEdit: false});
            return;
        }
        this.props.server.name = newVal;
        swal.showLoading();
        fetch(API_URL + "/servers/" + this.props.server.id, {
            method: "POST",
            body: FD,
            credentials: "include"
        }).then(e => e.json())
            .catch(e => console.error(e))
            .then(res => {
                this.setState({nameEdit: false});
                swal(
                    'Edited',
                    'The name was edited successfully.',
                    'success'
                )
            });
    }
    delete() {
        swal({
            title: 'Delete this server?',
            text: 'Are you sure you want to delete ' +this.props.server.name + '?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "Go for it, bossman",
            cancelButtonText: "Nah fam",
            footer: "Servers cannot be recovered, no matter how much you beg.",
            focusCancel: true,
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return fetch(API_URL + "/servers/" + this.props.server.id, {
                    method: "DELETE",
                    credentials: "include"
                }).then(e => {
                    this.setState({deleted: true});
                    return true;
                }).catch(e => console.error(e));
            }
        }).then((result) => {
            if(!result.value) return;
            swal({title: 'Server deleted.',
                text: this.props.server.name + ' was successfully deleted',
                type: 'success'});
        });
    }
    render() {
        if(this.state.deleted) return null;
        if(!this.props.publicList) return <tr>
            <td><EditableField text={this.props.server.name} editable={this.state.nameEdit} OnChange={this.changed} /></td>
            <td>{this.props.server.created_at}</td>
            <td>
                <div className={"d-flex justify-content-end"}>
                    <Button size={"sm"} color={"primary"} onClick={() => this.setState({nameEdit: true})}>Edit</Button>
                    <Button size={"sm"} color={"danger"} className={"ml-1"} onClick={this.delete}>Delete</Button>
                </div>
            </td>
        </tr>;
        return <tr>
            <td>{this.props.server.name}</td>
        </tr>
    }
}

export default Server;