import React, {Component} from 'react';
import Server from "./Server";
import {Table} from 'reactstrap';

class ServerList extends Component {
    render() {
        let servs = [];
        this.props.servers.forEach((e) => {
            servs.push(<Server key={e.id} server={e} publicList={this.props.publicList} />)
        });
        return (
            <Table>
                <thead>
                {!this.props.publicList ? <tr>
                        <th>Server Name</th>
                        <th>Added on</th>
                        <th></th>
                    </tr> : <tr>
                        <th>Available Servers</th>
                    </tr>}
                </thead>
                <tbody>
                    {servs}
                </tbody>
            </Table>
        );
    }
}

export default ServerList;